# RLI-CLI #

ARCHIVED. Rocket Lift has ceased operations.

The __R__ocket__L__ift (__I__nc.) __C__ommand-__L__ine __I__nterface.

## What? Help! ##

This is a collection of command-line tools and utilities used by Rocket Lift.

If you've gotten it installed:

 * Run `rli help` for some helpful help info.
 * `rli help <command>` will show some brief command-specific info.
 * `rli <command> --help` and similar should print the command's own built-in help and usage info.

__Note__: We're using a version of [Basecamp's "sub" framework](https://github.com/basecamp/sub) to hold this bundle together. If you come across text akin to "run 'sub help' for...", subsitute "rli" for "sub". We'll be cleaning that stuff up at some point.

## Installation & Updating ##

### From Source ###

So you use the command-line, and you do things at Rocket Lift... that means you have `git`... which means you can get this suite of goodies the same way you get any other code of ours.

 1. `cd` to a good folder like `~/RocketLift/bin`
 1. `git clone git@bitbucket.org:rocketlift/rli-cli.git rli-cli`
 1. Tune into your chosen update channel:
    - Stable: `git checkout builds/stable`
    - Risky: `git checkout builds/canary`

To update, run `git pull --force` periodically. To make changes or additions to this, `git clone` a separate development copy, elsewhere in your system, and make with the developing.

Make sure the good folder you chose earlier, especially this program's main entry-point, is in your shell environment's `$PATH`. For example, add this line...

```
export PATH="~/RocketLift/bin/rli-cli/bin:$PATH"
```

...to your `.bashrc` or `.bash_profile`.
