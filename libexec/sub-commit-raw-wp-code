#!/usr/bin/env bash
# Usage: rli commit-raw-wp-code
# Summary: make atomic commits for each WordPress component.
# Help: there are no arguments or options. The script is naive.
#
# Each commit has a hard-coded "Updates to <COMPONENT>" message. It
# does not differentiate between component additions and changes. Be
# sure to review your commits and 'git rebase -i' through them as
# needed before pushing.
#
# Source: RLI-CLI
#
# - Code: https://bitbucket.org/rocketlift/rli-cli
# - Tasks: https://app.asana.com/0/72035601086856/list

plugins=(wp-content/plugins/*)
themes=(wp-content/themes/*)

echo "-> Normalizing line-endings..."
git stash && git stash pop
echo

function commitComponent() {
	local OBJ="$1"
	echo "Working on $OBJ..."
	git add --all "$OBJ"
	git commit -q -m "Updates to $OBJ."
	sleep 1s
	echo
}

echo "-> Committing plugins..."
for pluginPath in "${plugins[@]}"; do
	commitComponent "$pluginPath"
done
echo

echo "-> Committing themes..."
for themePath in "${themes[@]}"; do
	commitComponent "$themePath"
done
echo

echo "-> Committing WP Core..."
git add --all "./*"
git commit -q -m "Updates to WordPress Core."
echo
