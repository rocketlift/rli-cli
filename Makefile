# Do things with GNU Make
# https://www.gnu.org/software/make/

.PHONY: help init clean


# == Variables == #
#
# Only set variables here that are specific to and required for Make to
# work. For everything else, set them in the environment.

env_source=env-example
env_file=.env
build_dir=dist

# == Help Documentation == #
#
# Print out this file's usage info. Whitespace (tabs for indenting, and
# spaces for alignment) matter here. Character-literals get printed to
# the shell, so make sure your fancy text editor doesn't do anything
# unexpected.
#
# Follow the same "repository life-cycle" order that commands themselves
# do. Setup stuff first, then development tasks, building, shipping, and
# finally, cleanup.

define HELPMSG

Execute common tasks for the '$(PROJECT_NAME)' project.

Commands:

	help - Show this message.

	watch - Automatically run tests, compilation, and other tasks when
	        any relevant files change.

	build - Commit code with populated dependencies to a special branch.

	ship-* - This project has a couple of targets to ship builds to. All
	         'make ship-<target>' commands should include building and
	         committing/pushing steps.
	
		Sub-commands:

			ship-canary - The "unstable" channel, on branch 'builds/canary'.
			ship-stable - The "stable" channel, on branch 'builds/stable'.

	clean - Un-populate dependencies, shut down servies, and otherwise
	        return the repo to a fresh and clean state.

	        Note: this doesn't include 'git config --local' settings.

endef
export HELPMSG

help:
	@echo "$$HELPMSG" | $${PAGER:-less}

watch:
	@echo "Watching all the things for you..."
	# https://github.com/cortesi/modd/issues/8
	GODEBUG=cgocheck=0 modd

build:
	@echo "Building..."
	make clean
	peru sync --no-overrides
	rsync -av --delete-after --exclude-from=.buildignore ./* $(build_dir)/

ship-canary:
	@echo "Shipping a 'canary' build..."
	make build
	./bin/rli git-directory-deploy $(build_dir) builds/canary origin

ship-stable:
	@echo "Shipping a 'stable' build..."
	make build
	./bin/rli git-directory-deploy $(build_dir) builds/stable origin


# == Repository Cleanup == #
#
# Cover everything, including de-populating dependencies, de-provisioning
# local virtual machines, shutting down daemons, etc.
#
# Cleanup should run in the opposite order as inititialization tasks, so
# that 'make clean' "unwinds" what 'make init' sets up. And like 'init'
# tasks, these should be idempotent.
#
# Note: we intentionally don't touch what the 'config' task sets up.

clean:
	@echo "Returning this world to a clean state..."
	peru clean
	rm -rf $(build_dir)

